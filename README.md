A two-factor login system based on a zero knowledge proof (ZKP) derived from ElGamal encryption.

Visit the page here:
https://tfzkp.herokuapp.com